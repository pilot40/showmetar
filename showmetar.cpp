
#if IBM
#include <windows.h>
#endif
#if LIN
#include <GL/gl.h>
#else
#if __GNUC__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#endif

#define XPLM200 = 1; // This example requires SDK2.0

#include "XPLMPlugin.h"
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMProcessing.h"
#include "XPLMDataAccess.h"
#include "XPLMMenus.h"
#include "XPLMUtilities.h"
#include "XPWidgets.h"
#include "XPStandardWidgets.h"
#include "XPLMCamera.h"
#include "XPUIGraphics.h"
#include "XPWidgetUtils.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

XPLMDataRef gMetarDataRef = NULL; // Our custom dataref
int gMetarValue; // Our custom dataref's value
int GetCounterDataRefCB(void* inRefcon);
void SetCounterDataRefCB(void* inRefcon, int outValue);
XPLMCommandRef MetarToggleCommand = NULL; // Our custom commands
int MetarToggleCommandHandler(XPLMCommandRef inCommand, // Our two custom command handlers
	                         XPLMCommandPhase inPhase,
	                         void * inRefcon);

static int MenuItem1;
static XPWidgetID ShowMetarWidget;
static XPWidgetID cap, metarQueryInput;
static XPWidgetID metarQueryButton, metarQueryOutput;
static XPLMMenuID id;
static void ShowMetarMenuHandler(void *, void *);
static void CreateShowMetarWidget(int x1, int y1, int w, int h);


// Handle any widget messages
static int ShowMetarWidgetHandler(
	XPWidgetMessage inMessage,
	XPWidgetID inWidget,
	intptr_t inParam1,
	intptr_t inParam2);

/*------------------------------------------------------------------------*/

PLUGIN_API int XPluginStart(
	char * outName,
	char * outSig,
	char * outDesc)
{
	int item;

	strcpy(outName, "Show Metar");
	strcpy(outSig, "pilot40.Show Metar");
	strcpy(outDesc, "Show Metar.");

	// Create our custom integer dataref
	gMetarDataRef = XPLMRegisterDataAccessor("MTR/Open",
		xplmType_Int, // The types we support
		1, // Writable
		GetCounterDataRefCB, SetCounterDataRefCB, // Integer accessors
		NULL, NULL, // Float accessors
		NULL, NULL, // Doubles accessors
		NULL, NULL, // Int array accessors
		NULL, NULL, // Float array accessors
		NULL, NULL, // Raw data accessors
		NULL, NULL); // Refcons not used
		// Find and intialize our Counter dataref
	gMetarDataRef = XPLMFindDataRef("MTR/Open");
	XPLMSetDatai(gMetarDataRef, 0);

	// Create our commands; these will increment and decrement our custom dataref.
	MetarToggleCommand = XPLMCreateCommand("MTR/Open", "MetarToggle");

	// Register our custom commands
	XPLMRegisterCommandHandler(MetarToggleCommand, // in Command name
		MetarToggleCommandHandler, // in Handler
		1, // Receive input before plugin windows.
		(void *)0); // inRefcon.

	// Build menu
	item = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "Show Metar", NULL, 1);

	id = XPLMCreateMenu("Show Metar", XPLMFindPluginsMenu(), item, ShowMetarMenuHandler, NULL);
	XPLMAppendMenuItem(id, "Open", (void *)"Open", 1);

	// Used by widget to make sure only one widgets instance created
	MenuItem1 = 0;
	return 1;
}

PLUGIN_API void XPluginStop(void)
{
	// Clean up
	XPLMUnregisterDataAccessor(gMetarDataRef);
	XPLMUnregisterCommandHandler(MetarToggleCommand, MetarToggleCommandHandler, 0, 0);
	XPLMDestroyMenu(id);
	if (MenuItem1 == 1)
	{
		XPDestroyWidget(ShowMetarWidget, 1);
		MenuItem1 = 0;
	}
}

PLUGIN_API void XPluginDisable(void)
{
}

PLUGIN_API int XPluginEnable(void)
{
	return 1;
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, int inMsg, void * inParam)
{
}

int GetCounterDataRefCB(void* inRefcon)
{
	return gMetarValue;
}

void SetCounterDataRefCB(void* inRefcon, int inValue)
{
	gMetarValue = inValue;
}
int MetarToggleCommandHandler(XPLMCommandRef inCommand,
	XPLMCommandPhase inPhase,
	void * inRefcon)
{
	// If inPhase == 0 the command is executed once on button down.
	if (inPhase == 0)
	{
		if (MenuItem1 == 0)
		{
			CreateShowMetarWidget(100, 480, 550, 120);
			XPShowWidget(ShowMetarWidget);
			MenuItem1 = 1;
		}
		else
			if (MenuItem1 == 1)
			{
				XPHideWidget(ShowMetarWidget);
				MenuItem1 = 0;
			}
	}
	// Return 1 to pass the command to plugin windows and X-Plane.
	// Returning 0 disables further processing by X-Plane.

	return 0;
}
// Handle any menu messages, only one, to created widget dialog
void ShowMetarMenuHandler(void * mRef, void * iRef)
{
	if (!strcmp((char *)iRef, "Open"))
	{
		if (MenuItem1 == 0)
		{
			CreateShowMetarWidget(100, 480, 550, 120);
			XPShowWidget(ShowMetarWidget);
			MenuItem1 = 1;
		}
		else
			if (MenuItem1 == 1)
			{
				XPHideWidget(ShowMetarWidget);
				MenuItem1 = 0;
			}
	}
}
// This creates the widgets dialog and any controls
void CreateShowMetarWidget(int x, int y, int w, int h)
{
	int x2 = x + w;
	int y2 = y - h;

	ShowMetarWidget = XPCreateWidget(x, y, x2, y2,
		1, // Visible
		"METAR Request", // desc
		1, // root
		NULL, // no container
		xpWidgetClass_MainWindow);

	XPSetWidgetProperty(ShowMetarWidget, xpProperty_MainWindowType, xpMainWindowStyle_Translucent);
	XPSetWidgetProperty(ShowMetarWidget, xpProperty_MainWindowHasCloseBoxes, 1);

	x += 10;
	y -= 20;
	cap = XPCreateWidget(x, y, x + 40, y - 20, 1, " Airport ICAO code:", 0, ShowMetarWidget, xpWidgetClass_Caption);

	XPSetWidgetProperty(cap, xpProperty_CaptionLit, 1);

	y -= 20;
	// Airport input
	metarQueryInput = XPCreateWidget(x + 5, y, x + 120, y - 20, 1, "", 0, ShowMetarWidget, xpWidgetClass_TextField);

	XPSetWidgetProperty(metarQueryInput, xpProperty_TextFieldType, xpTextEntryField);
	XPSetWidgetProperty(metarQueryInput, xpProperty_Enabled, 1);
	XPSetWidgetProperty(metarQueryInput, xpProperty_TextFieldType, xpTextTranslucent);

	// QueryButton
	metarQueryButton = XPCreateWidget(x + 140, y, x + 210, y - 20, 1, "Request", 0, ShowMetarWidget, xpWidgetClass_Button);
	XPSetWidgetProperty(metarQueryButton, xpProperty_ButtonType, xpPushButton);
	XPSetWidgetProperty(metarQueryButton, xpProperty_Enabled, 1);

	y -= 20;
	//Help caption
	cap = XPCreateWidget(x, y, x + 300, y - 20, 1, "METAR:", 0, ShowMetarWidget, xpWidgetClass_Caption);
	XPSetWidgetProperty(cap, xpProperty_CaptionLit, 1);

	y -= 20;
	//Query output
	metarQueryOutput = XPCreateWidget(x + 5, y, x + 530, y - 20, 1, "", 0, ShowMetarWidget, xpWidgetClass_TextField);
	XPSetWidgetProperty(metarQueryOutput, xpProperty_TextFieldType, xpTextEntryField);
	XPSetWidgetProperty(metarQueryOutput, xpProperty_Enabled, 1);
	XPSetWidgetProperty(metarQueryOutput, xpProperty_TextFieldType, xpTextTranslucent);
	XPAddWidgetCallback(ShowMetarWidget, ShowMetarWidgetHandler);
	XPSetKeyboardFocus(metarQueryInput);
}

// Handle any widget messages
int ShowMetarWidgetHandler(
	XPWidgetMessage inMessage,
	XPWidgetID inWidget,
	intptr_t inParam1,
	intptr_t inParam2)
{
	char Buffer[256];

	// Close button pressed, only hide the widget, rather than destropying itbbbbbbbbb.
	if (inMessage == xpMessage_CloseButtonPushed)
	{
		if (MenuItem1 == 1)
		{
			XPHideWidget(ShowMetarWidget);
			MenuItem1 = 0;
		}
		return 1;
	}
	// Test for a button pressed
	if (inMessage == xpMsg_PushButtonPressed)
	{
		if (inParam1 == (intptr_t)metarQueryButton)
		{

			XPGetWidgetDescriptor(metarQueryInput, Buffer, sizeof(Buffer));
			int query = strlen(Buffer);
			if (query == 4)
			{
				ifstream input("./././METAR.rwx", ios::in);
				char buf[1024];
				while (input.getline(buf, 1024)) {
					if (strstr(buf, Buffer))
						XPSetWidgetDescriptor(metarQueryOutput, buf);
				}
			}
			else

				XPSetWidgetDescriptor(metarQueryOutput, "Please insert a valid ICAO code!");
		}

		return 1;
	}

	return 0;
}

